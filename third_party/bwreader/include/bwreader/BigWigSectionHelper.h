

int32_t extractSectionData(RPChromosomeRegion& selectionRegion, std::vector<char>& sectionBuffer,
                           std::map<uint32_t, std::string>& chromosomeMap,bool contained,
                            std::vector<WigItem>& wigItemList);

